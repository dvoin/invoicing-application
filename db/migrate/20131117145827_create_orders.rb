class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :client_name
      t.text :shipping_address

      t.timestamps
    end
  end
end
