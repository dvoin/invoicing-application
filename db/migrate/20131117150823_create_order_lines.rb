class CreateOrderLines < ActiveRecord::Migration
  def change
    create_table :order_lines do |t|
      t.integer :order_id
      t.string :article_id
      t.decimal :quantity

      t.timestamps
    end
  end
end
