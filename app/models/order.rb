class Order < ActiveRecord::Base
    #DVO
	has_many :order_lines, inverse_of: :order, dependent: :destroy #to assure that when a Order is destroyed, the order_lines are alos destroyed

	#This creates an order_lines_attributes= method on Order that allows you to create, update and (optionally) destroy order_lines.
	#metoda parametrii symbol, hash
	accepts_nested_attributes_for :order_lines, :allow_destroy => true 
    #DVO
end
