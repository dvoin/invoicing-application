class OrderLine < ActiveRecord::Base
	belongs_to :order , inverse_of: :order_lines #DVO: necessary?? 
	belongs_to :article
	# has_one :article #nu e mai bine sa folosesc has_one? Fiecare OrderLine are un articol

	# default_scope -> { order('created_at DESC') }

	# validates :order_id, presence: true
	validates :quantity, presence: true
	before_save :sync_with_article

	def sync_with_article
		self.article_price=self.article.price
		self.article_title=self.article.title
	end
end
