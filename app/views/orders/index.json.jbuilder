json.array!(@orders) do |order|
  json.extract! order, :client_name, :shipping_address
  json.url order_url(order, format: :json)
end
